﻿#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include<stdio.h>

#include <iostream>
#include <conio.h>
#include <string>
#include "fstream"
#include "stdio.h"
#include<ctime>
#include "Windows.h"

/*#ifdef _DEBUG
#endif*/


using namespace std;


const int n = 6000;


__global__
void Matrix_filling(int *matrix, float *x, float *y, int *n, int* h, int* w, int *m, int *r) {

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	int ii = threadIdx.x;

	__shared__ int ln;

	if (ii == 0) { ln = (int)((*w)*(*m) / (2 * (*r))); }

	__syncthreads();

	__shared__ int xi[1024];
	__shared__ int yi[1024];


	xi[ii] = (int)((x[i] - (*r) + (*w)*(*m) / 2) / (2 * (*r)));
	yi[ii] = (int)((y[i] - (*r) + (*h)*(*m) / 2) / (2 * (*r)));


	if (i < (*n)) {
		matrix[yi[ii] * ln + xi[ii]] = i;
	}


}


__global__
void Density(int *matrix, int *quantity, float *q, float *x, float *y, int* n, int* Length, int* h, int* w, int *m, int *r) {

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	int jj = threadIdx.x;
	int xx = threadIdx.y;
	int yy = threadIdx.z;
	int pos = blockDim.z*yy + xx;

	__shared__ int ln;

	if ((jj == 0) && (xx == 0) && (yy == 0)) { ln = (int)((*w)*(*m) / (2 * (*r))); }

	__syncthreads();

	int x0 = (int)((x[i] - (*r) + (*w)*(*m) / 2) / (2 * (*r)));
	int y0 = (int)((y[i] - (*r) + (*h)*(*m) / 2) / (2 * (*r)));


	int xi = x0 - (blockDim.y - 1) / 2 + xx;
	int yi = y0 - (blockDim.z - 1) / 2 + yy;

	int ind = yi * ln + xi;
	ind = matrix[ind];

	__shared__ float l[6][49];
	__shared__ float q0[6][49];

	if ((i < (*n)) && (ind != -1) && (xi < (*w)*(*m) / (2 * (*r))) && (xi >= 0) && (yi < (*h)*(*m) / (2 * (*r))) && (yi >= 0)) {


		l[jj][pos] = hypotf((x[i] - x[ind]), (y[i] - y[ind]));

		if (l[jj][pos] < (*Length)) {

			q0[jj][pos] = 10 * powf((powf((*Length), 2) - powf(l[jj][pos], 2)) / 30625, 3);
			atomicAdd(q + i, q0[jj][pos]);
			atomicAdd(quantity + i, 1);
		}


	}

}


__global__
void Forces(int *matrix, int *quantity, float *q, float *x, float *y, float *vx, float *vy, int* n, int* Length, float* dt, int* h, int* w, int *m, int *r) {

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	int jj = threadIdx.x;
	int xx = threadIdx.y;
	int yy = threadIdx.z;
	int pos = blockDim.z*yy + xx;

	__shared__ int ln;

	if ((jj == 0) && (xx == 0) && (yy == 0)) { ln = (int)((*w)*(*m) / (2 * (*r))); }

	__syncthreads();

	int x0 = (int)((x[i] - (*r) + (*w)*(*m) / 2) / (2 * (*r)));
	int y0 = (int)((y[i] - (*r) + (*h)*(*m) / 2) / (2 * (*r)));

	int loc = y0 * ln + x0;
	loc = matrix[loc];

	int xi = x0 - (blockDim.y - 1) / 2 + xx;
	int yi = y0 - (blockDim.y - 1) / 2 + yy;

	int ind = yi * ln + xi;
	ind = matrix[ind];

	__shared__ float l[6][49];
	__shared__ float a[6][49];

	__shared__ float q0[6][49];

	if ((i < (*n)) && (ind != -1) && (xi < (*w)*(*m) / (2 * (*r))) && (xi >= 0) && (yi < (*h)*(*m) / (2 * (*r))) && (yi >= 0)) {



		l[jj][pos] = hypotf((x[i] - x[ind]), (y[i] - y[ind]));

		if ((l[jj][pos] < (*Length)) && (l[jj][pos] != 0)) {

			int q1 = 24;
			int q2 = 24;

			if ((quantity[i] >= 5) && (quantity[i] <= 8)) { q1 = quantity[i] * 1.3 + 12.3; }
			if ((quantity[ind] >= 5) && (quantity[ind] <= 8)) { q2 = quantity[ind] * 1.3 + 12.3; }

			if ((quantity[i] >= 2) && (quantity[i] <= 4)) { q1 = (quantity[i] - 2)*2.25 + 12; }
			if ((quantity[ind] >= 2) && (quantity[ind] <= 4)) { q2 = (quantity[ind] - 2)*2.25 + 12; }


			q0[jj][pos] = q1 + q2;


			a[jj][pos] = -40 * powf(((*Length) - l[jj][pos]) / 3, 3)*(q[i] + q[ind] - q0[jj][pos])*(*dt) / (l[jj][pos] * q[i]);  //-a*dt

			atomicAdd(vx + i, a[jj][pos] * (x[ind] - x[i]));
			atomicAdd(vy + i, a[jj][pos] * (y[ind] - y[i]));

			if (loc != i)
			{
				atomicAdd(vx + ind, -a[jj][pos] * (x[ind] - x[i]));
				atomicAdd(vy + ind, -a[jj][pos] * (y[ind] - y[i]));
			}


			__syncthreads();


			a[jj][pos] = 3000 * (-0.5*powf(l[jj][pos] / (*Length), 3) + powf(l[jj][pos] / (*Length), 2) + 0.5*(*Length) / l[jj][pos] - 1)*(*dt) / q[i];  //a*dt ������������

			atomicAdd(vx + i, a[jj][pos] * (vx[ind] - vx[i]));
			atomicAdd(vy + i, a[jj][pos] * (vy[ind] - vy[i]));

			if (loc != i)
			{
				atomicAdd(vx + ind, -a[jj][pos] * (vx[ind] - vx[i]));
				atomicAdd(vy + ind, -a[jj][pos] * (vy[ind] - vy[i]));
			}
		}

	}

}


__global__
void XY(float *q, float *x, float *y, float *vx, float *vy, bool wall1, bool wall2, float x0, int* h, int* w, int* r, int* m, int* k, float* dt) {

	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < n) {

		if (y[i] > *h / 2 * (*m) - *r) { vy[i] -= *k*(y[i] - *h / 2 * (*m) + *r)*(*dt); }

		if (y[i] < -*h / 2 * (*m) + *r) { vy[i] += *k*(-y[i] - *h / 2 * (*m) + *r)*(*dt); }

		/*if (x[i]>-*w/2*(*m)+x0 && x[i]<-*w/2*(*m)+*r+x0) { vx[i]+=*k*100*(-x[i]-*w/2*(*m)+*r+x0)*(*dt);}
		if (x[i]<-*w/2*(*m)+x0 && x[i]>-*w/2*(*m)-*r+x0) {vx[i]-=*k*100*(x[i]+*w/2*(*m)+*r-x0)*(*dt);}


		if (x[i]>+*w/2*(*m) && x[i]<+*w/2*(*m)+*r) { vx[i]+=*k*100*(-x[i]+*w/2*(*m)+*r)*(*dt);};
		if (x[i]<+*w/2*(*m) && x[i]>+*w/2*(*m)-*r) {vx[i]-=k*100*(x[i]-*w/2*(*m)+*r)*(*dt);};*/

		if (x[i] < -*w / 2 * (*m) + *r + x0) { vx[i] += (*k)*(-x[i] - *w / 2 * (*m) + *r + x0)*(*dt); }

		if (x[i] > +*w / 2 * (*m) - *r) { vx[i] -= (*k)*(x[i] - *w / 2 * (*m) + *r)*(*dt); }



		vy[i] -= 98100 * (*dt);

		//	vx[i]=vx[i]*0.997;        
		//	vy[i]=vy[i]*0.997;

		x[i] = fmaf(vx[i], (*dt), x[i]);
		y[i] = fmaf(vy[i], (*dt), y[i]);

	}

}


void Free_var(int* d_quantity, int* d_matrix, float* d_q, float* d_x, float* d_y, float* d_vx, float* d_vy, int* d_n, int* d_h, int *d_w, int* d_k, float* d_dt, int* d_r, int* d_m, int* d_E, int* d_Length,
	int* d_fps, float* d_p1, float* d_p2, int* d_Radius, float* q, float* x, float* y, float* vx, float* vy, FILE *f)
{
	cout << "Could not copy!" << endl;
	cudaFree(d_quantity);
	cudaFree(d_matrix);
	cudaFree(d_q);
	cudaFree(d_x);
	cudaFree(d_y);
	cudaFree(d_vx);
	cudaFree(d_vy);

	cudaFree(d_n);
	cudaFree(d_h);
	cudaFree(d_w);
	cudaFree(d_k);
	cudaFree(d_dt);
	cudaFree(d_r);
	cudaFree(d_m);
	cudaFree(d_E);
	cudaFree(d_Length);
	cudaFree(d_fps);
	cudaFree(d_p1);
	cudaFree(d_p2);
	cudaFree(d_Radius);


	delete[] q;
	delete[] x;
	delete[] y;
	delete[] vx;
	delete[] vy;

	fclose(f);

}



int main() {

	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);      //   Used for checking memory leaks

	srand(time(NULL));

	bool wall1 = true;
	bool wall2 = true;
	int nap = 1;
	double t = 0;
	int i = 0;
	float x0 = 0;
	float v = 0;

	string s;
	bool closef = false;



	float *x = new float[n];
	float *y = new float[n];
	float *vx = new float[n];
	float *vy = new float[n];
	float *q = new float[n];

	float *d_x;
	float *d_y;
	float *d_vx;
	float *d_vy;
	float *d_q;

	int *d_quantity;




	int h = 600;
	int w = 1000;
	int k = 10000000;
	float dt = 0.00005F;
	int r = 50;
	int m = 20;
	int E = 50000000;
	int Length = 175;
	int fps = 40;
	float p1 = 3;
	float p2 = 5;
	int Radius = 25;




	int Msize = w * h*m*m / (4 * Radius*Radius);


	int *d_matrix;



	int *d_n;
	int *d_h;
	int *d_w;
	int *d_k;
	float *d_dt;
	int *d_r;
	int *d_m;
	int *d_E;
	int *d_Length;
	int *d_fps;
	float *d_p1;
	float *d_p2;
	int *d_Radius;



	cudaMalloc(&d_n, sizeof(int));
	cudaMalloc(&d_h, sizeof(int));
	cudaMalloc(&d_w, sizeof(int));
	cudaMalloc(&d_k, sizeof(int));
	cudaMalloc(&d_dt, sizeof(float));
	cudaMalloc(&d_r, sizeof(int));
	cudaMalloc(&d_m, sizeof(int));
	cudaMalloc(&d_E, sizeof(int));
	cudaMalloc(&d_Length, sizeof(int));
	cudaMalloc(&d_fps, sizeof(int));
	cudaMalloc(&d_p1, sizeof(float));
	cudaMalloc(&d_p2, sizeof(float));
	cudaMalloc(&d_Radius, sizeof(int));

	cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_h, &h, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_w, &w, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_k, &k, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_dt, &dt, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_r, &r, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_m, &m, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_E, &E, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_Length, &Length, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_fps, &fps, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_p1, &p1, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_p2, &p2, sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_Radius, &Radius, sizeof(int), cudaMemcpyHostToDevice);


	memset(q, 0, sizeof(float)*n);


	ifstream save;                                 //file with result of calculations
	save.open("save.txt");
	for (int j = 0; j < n; j++) {
		save >> x[j] >> y[j] >> vx[j] >> vy[j];
	}
	save.close();


	FILE *f;
	f = fopen("voda.txt", "w");

	fprintf(f, "%i\n%i\n%i\n", h, w, n);



	if (cudaMalloc(&d_quantity, sizeof(int)*n) != cudaSuccess)
	{
		Free_var(NULL, NULL, NULL, NULL, NULL, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_matrix, sizeof(int)*Msize) != cudaSuccess)
	{
		Free_var(d_quantity, NULL, NULL, NULL, NULL, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_q, sizeof(float)*n) != cudaSuccess)
	{
		Free_var(d_quantity, d_matrix, NULL, NULL, NULL, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_x, sizeof(float)*n) != cudaSuccess)
	{
		Free_var(d_quantity, d_matrix, d_q, NULL, NULL, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_y, sizeof(float)*n) != cudaSuccess)
	{
		Free_var(d_quantity, d_matrix, d_q, d_x, NULL, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_vx, sizeof(float)*n) != cudaSuccess)
	{
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, NULL, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMalloc(&d_vy, sizeof(float)*n) != cudaSuccess)
	{
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, NULL, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMemcpy(d_q, q, sizeof(float)*n, cudaMemcpyHostToDevice) != cudaSuccess) {
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMemcpy(d_x, x, sizeof(float)*n, cudaMemcpyHostToDevice) != cudaSuccess) {
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMemcpy(d_y, y, sizeof(float)*n, cudaMemcpyHostToDevice) != cudaSuccess) {
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMemcpy(d_vx, vx, sizeof(float)*n, cudaMemcpyHostToDevice) != cudaSuccess) {
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}

	if (cudaMemcpy(d_vy, vy, sizeof(float)*n, cudaMemcpyHostToDevice) != cudaSuccess) {
		Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
		return 0;
	}



	dim3 threads(6, 7, 7);     //n*n
	dim3 block(1000);

	dim3 th(1024);          //n
	dim3 bl(6);

	do {
		t = t + dt;
		i = i + 1;

		v = v + 1;



		if (wall2 == true) {
			x0 = x0 + nap * v*dt;
		}

		/*if ((x0>=3000) && (nap==1)) {
		nap=-1;
		v=0;
		}
		if ((nap==-1) && (x0<=0)) {
		wall2=false;
		wall1=false;
		x0=(int)x0;
		}*/

		if ((x0 >= 3000) && (nap == 1)) {
			wall2 = false;
			wall1 = false;
			x0 = floor(x0);
		}

		//if (v>15000) wall1=false;


		cudaMemset(d_matrix, -1, sizeof(int)*Msize);

		cudaMemset(d_q, 0, sizeof(float)*n);

		cudaMemset(d_quantity, 0, sizeof(int)*n);



		Matrix_filling << <bl, th >> > (d_matrix, d_x, d_y, d_n, d_h, d_w, d_m, d_Radius);

		Density << <block, threads >> > (d_matrix, d_quantity, d_q, d_x, d_y, d_n, d_Length, d_h, d_w, d_m, d_Radius);

		Forces << <block, threads >> > (d_matrix, d_quantity, d_q, d_x, d_y, d_vx, d_vy, d_n, d_Length, d_dt, d_h, d_w, d_m, d_Radius);    //blocks

		XY << <bl, th >> > (d_q, d_x, d_y, d_vx, d_vy, wall1, wall2, x0, d_h, d_w, d_r, d_m, d_k, d_dt);


		if (i == fps) {

			if (cudaMemcpy(x, d_x, sizeof(float)*n, cudaMemcpyDeviceToHost) != cudaSuccess) {
				Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
				return 0;
			}

			if (cudaMemcpy(y, d_y, sizeof(float)*n, cudaMemcpyDeviceToHost) != cudaSuccess) {
				Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
				return 0;
			}

			if (cudaMemcpy(vx, d_vx, sizeof(float)*n, cudaMemcpyDeviceToHost) != cudaSuccess) {
				Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
				return 0;
			}

			if (cudaMemcpy(vy, d_vy, sizeof(float)*n, cudaMemcpyDeviceToHost) != cudaSuccess) {
				Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);
				return 0;
			}

			cout << t << "\n";

			fprintf(f, "%f ", floor(t * 10000) / 10000);

			for (int j = 0; j < n - 1; j++) {
				fprintf(f, "%i %i %i %i ", (int)(x[j] / m), (int)(y[j] / m), (int)(vx[j] / m), (int)(vy[j] / m));
			}
			fprintf(f, "%i %i %i %i\n", (int)(x[n - 1] / m), (int)(y[n - 1] / m), (int)(vx[n - 1] / m), (int)(vy[n - 1] / m));

			i = 0;

			if (GetAsyncKeyState(112)) {
				cin >> s;
				if (s == "s") {
					ofstream save;
					save.open("save.txt");
					for (int j = 0; j < n; j++) {
						save << (int)x[j] << " " << (int)y[j] << " " << (int)vx[j] << " " << (int)vy[j] << "\n";
					}
					save.close();
				}
				if (s == "c") { closef = true; }
				if (s == "wall1") {
					cin >> s;
					if (s == "t") { wall1 = true; }
					if (s == "f") { wall1 = false; }
				}
				if (s == "wall2") {
					cin >> s;
					if (s == "t") { wall2 = true; }
					if (s == "f") { wall2 = false; }
				}
			}

		}


	} while (closef == false);


	Free_var(d_quantity, d_matrix, d_q, d_x, d_y, d_vx, d_vy, d_n, d_h, d_w, d_k, d_dt, d_r, d_m, d_E, d_Length, d_fps, d_p1, d_p2, d_Radius, q, x, y, vx, vy, f);


	return 0;
}