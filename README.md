In this project implemented fluid flows simulation, in particular, soliton propagation.<br />
Here you can see [example of soliton propagation](https://drive.google.com/open?id=0B5lf5-TNbDbYanpEOEFYZU5XUHc)

To run the calculations, you need Nvidia 430.86 drivers or newer.<br />
To start the calculations, open the file WaterCUDA.exe. In the console you will see the number of calculated seconds of simulation.

To view the animation of the calculated behavior, open the file Projectrun.exe.